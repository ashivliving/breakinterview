<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
      protected $fillable = [
        'title', 'body', 'opt1', 'opt2', 'opt3', 'opt4', 'ans',
    ];

    protected $table = 'question';

     public static $create_validation_rules = [
	  	'title'=>'required',
	    'opt1'=>'required',
	    'opt2'=>'required',
	    'opt3'=>'required',
	    'opt4'=>'required'//,
	    //'ans'=>'required'
	  ];
}
