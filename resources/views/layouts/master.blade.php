<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Break Interview | Question Bank</title>
	<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
<body>

<div class="container">
	@yield('content')
</div>
</body>
</html>
