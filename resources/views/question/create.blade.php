@extends('layouts.master')


@section('content')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h3>Create Question</h3>
      <hr/>
      {!! Form::open(array('route'=>'question.store')) !!}
        <div class="form-group">
          {!! Form::label('title','Question Title') !!}
          {!! Form::text('title',null,array('class'=>'form-control','placeholder'=>'Question')) !!}
        </div>

        <div class="form-group">
          {!! Form::label('body','Question body') !!}
          {!! Form::text('body',null,array('class'=>'form-control','placeholder'=>'Body')) !!}
        </div>

        <div class="form-group">
          {!! Form::label('opt1','1st Option') !!}
          {!! Form::text('opt1',null,array('class'=>'form-control','placeholder'=>'1st Option')) !!}
        </div>

        <div class="form-group">
          {!! Form::label('opt2','2nd Option') !!}
          {!! Form::text('opt2',null,array('class'=>'form-control','placeholder'=>'2nd Option')) !!}
        </div>

        <div class="form-group">
          {!! Form::label('opt3','3rd Option') !!}
          {!! Form::text('opt3',null,array('class'=>'form-control','placeholder'=>'3rd Option')) !!}
        </div>

        <div class="form-group">
          {!! Form::label('opt4','4th Option') !!}
          {!! Form::text('opt4',null,array('class'=>'form-control','placeholder'=>'4th Option')) !!}
        </div>

        <div class="form-group">
          <label>Answer</label>
          <select class="form-control input-sm" name="ans" id="ans">
            <option value="">Select Answer</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </div>

        <div class="form-group">
          {!! Form::token() !!}
          {!! Form::submit('Submit',array('class'=>'form-control btn btn-success')) !!}
        </div>

       
      {!! Form::close() !!}

@endsection
