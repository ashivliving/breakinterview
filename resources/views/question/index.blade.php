@extends('layouts.master')

@section('content')

  <hr>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
    	@if( count($quest)===0)
    		No Question available.
    	@else
       <div >
      <div class="hidden">{{$count =1}} </div>
      @foreach($quest as $que)
        <div class = "panel panel-success">
         <div class = "panel-heading">
            <h3 class = "panel-title">Q.{{$count++ }}) {{ link_to_route('question.show',$que->title,array($que->id)) }}</h3>
         </div>
         
         <div class = "panel-body">
         @if(!($que->body == NULL))
           <div class = "well well-sm"> {{$que->body}}</div>
         @endif

          <fieldset id="{{$que->id}}">
        <div class="radio">
          <label><input type="radio" name="{{$que->id}}">{{$que->opt1}}</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="{{$que->id}}">{{$que->opt2}}</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="{{$que->id}}">{{$que->opt3}}</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="{{$que->id}}">{{$que->opt4}}</label>
        </div>
        </fieldset>

      </div>
      
      </div>
      @endforeach
       <button class="form-control btn btn-success">Submit</button>
      @endif
    </div>
  </div>

@endsection